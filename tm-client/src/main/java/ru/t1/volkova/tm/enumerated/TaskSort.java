package ru.t1.volkova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.Comparator;

public enum TaskSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANSE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANSE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANSE::compare);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator<TaskDTO> comparator;

    TaskSort(
            @NotNull final String displayName,
            @NotNull final Comparator<TaskDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
