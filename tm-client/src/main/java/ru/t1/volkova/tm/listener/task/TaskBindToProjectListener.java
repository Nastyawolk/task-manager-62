package ru.t1.volkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    private static final String NAME = "task-bind-to-project";

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskID = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectID);
        request.setTaskId(taskID);
        getTaskEndpoint().bindTaskToProject(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
