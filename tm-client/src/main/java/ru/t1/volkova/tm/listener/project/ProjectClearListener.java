package ru.t1.volkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectClearRequest;
import ru.t1.volkova.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    private static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private static final String NAME = "project-clear";

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
