package ru.t1.volkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Start task status by id.";

    @NotNull
    private static final String NAME = "task-start-by-id";

    @Override
    @EventListener(condition = "@taskStartByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusById(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
