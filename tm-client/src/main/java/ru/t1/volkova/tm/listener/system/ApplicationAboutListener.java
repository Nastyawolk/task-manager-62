package ru.t1.volkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.volkova.tm.dto.response.system.ServerAboutResponse;
import ru.t1.volkova.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
