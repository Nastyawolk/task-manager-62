package ru.t1.volkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskClearRequest;
import ru.t1.volkova.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    private static final String NAME = "task-clear";

    @EventListener(condition = "@taskClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
