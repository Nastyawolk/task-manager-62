package ru.t1.volkova.tm.api.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.model.Project;

@Repository
public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

}
