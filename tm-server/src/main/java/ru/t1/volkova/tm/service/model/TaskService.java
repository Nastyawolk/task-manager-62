package ru.t1.volkova.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.ITaskRepository;
import ru.t1.volkova.tm.api.service.model.ITaskService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    @NotNull
    @Getter
    @Autowired
    public ITaskRepository repository;

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.getUser().setId(userId);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        repository.delete(task);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task task = repository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @NotNull
    public Task findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final List<Task> tasks = repository.findAllByUserId(userId);
        if (tasks == null) throw new ProjectNotFoundException();
        return tasks.get(index);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks = repository.findAllByUserIdAndProjectId(userId, projectId);
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks = repository.findAllByUserId(userId);
        return tasks;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId,
                              @Nullable final Comparator comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks =
                (List<Task>) repository.findAllByUserId(userId).stream()
                        .sorted(comparator).collect(Collectors.toList());
        if (tasks.isEmpty()) throw new TaskNotFoundException();
        return tasks;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

}
