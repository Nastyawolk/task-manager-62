package ru.t1.volkova.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.repository.model.IAbstractRepository;
import ru.t1.volkova.tm.api.service.model.IAbstractService;
import ru.t1.volkova.tm.model.AbstractModel;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

}
