package ru.t1.volkova.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.volkova.tm.api.service.model.IUserOwnedService;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

}
