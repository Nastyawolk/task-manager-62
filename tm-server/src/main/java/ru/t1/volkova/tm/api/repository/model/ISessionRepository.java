package ru.t1.volkova.tm.api.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.model.Session;

@Repository
public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {

}
