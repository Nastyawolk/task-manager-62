package ru.t1.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.dto.model.SessionDTO;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ISessionDTOService {

    void add(@NotNull SessionDTO entity);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    SessionDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId) throws SQLException;

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    void removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    @Transactional
    void clear(@Nullable String userId);

}
