package ru.t1.volkova.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectDTORepository extends IAbstractUserOwnedDTORepository<ProjectDTO> {

}
