package ru.t1.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.model.User;

@Repository
public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

}
