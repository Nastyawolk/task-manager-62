package ru.t1.volkova.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.dto.model.SessionDTO;

@Repository
public interface ISessionDTORepository extends IAbstractUserOwnedDTORepository<SessionDTO> {

}
