package ru.t1.volkova.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public  interface IAbstractDTORepository<E extends AbstractModelDTO> extends JpaRepository<E, String> {

}
