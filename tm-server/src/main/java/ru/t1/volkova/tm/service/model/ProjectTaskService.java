package ru.t1.volkova.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.ITaskRepository;
import ru.t1.volkova.tm.api.service.model.IProjectService;
import ru.t1.volkova.tm.api.service.model.IProjectTaskService;
import ru.t1.volkova.tm.api.service.model.ITaskService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.model.Task;

import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final ITaskRepository repository = taskService.getRepository();
            @NotNull final Task task = taskService.findOneById(userId, taskId);
            task.getProject().setId(projectId);
            repository.save(task);
            return task;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null || tasks.size() == 0) throw new TaskNotFoundException();
        for (@NotNull final Task task : tasks) {
            taskService.removeOneById(userId, task.getId());
        }
        projectService.removeOneById(userId, projectId);
    }

    @Override
    @NotNull
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final Task task = taskService.findOneById(userId, taskId);
        @NotNull final ITaskRepository repository = taskService.getRepository();
            task.getProject().setId(null);
            repository.save(task);
        return task;
    }

}
