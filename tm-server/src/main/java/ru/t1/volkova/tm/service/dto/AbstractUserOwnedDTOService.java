package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;

@Getter
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedDTOModel, R extends IAbstractUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

}
