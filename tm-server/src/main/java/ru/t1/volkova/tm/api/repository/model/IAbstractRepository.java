package ru.t1.volkova.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.volkova.tm.model.AbstractModel;

@NoRepositoryBean
public interface IAbstractRepository<E extends AbstractModel> extends JpaRepository <E, String> {

}
