package ru.t1.volkova.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    @SneakyThrows
    void receive(@NotNull MessageListener entityListener);

}
