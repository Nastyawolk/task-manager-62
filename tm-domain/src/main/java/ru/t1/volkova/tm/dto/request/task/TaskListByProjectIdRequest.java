package ru.t1.volkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

}
